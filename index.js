const express = require("express")
const cors = require("cors")
const fs = require('fs');
const { Server } = require("socket.io");
const { createServer } = require('node:http');
const ws =require('ws');

const { config } = require("./config")
const { logger } = require("./logger");
const { initialiseSocket, SocketClass } = require("./socket");

const app = express()

const convertToJson = (readdata, text, startDate, endDate) => {
    let array = readdata.split("\n")
    let dataArray = [];
    for (let i = 0; i < array.length; i++) {
        if (array[i] == '') { continue }
        dataArray.push(JSON.parse(array[i]))
    };
    if(text) {
        dataArray =dataArray.filter(it => {
            if(it.log.body.toLowerCase().includes(text.toLowerCase()) || it.log.service.toLowerCase().includes(text.toLowerCase()) || it.log.severity.toLowerCase().includes(text.toLowerCase())) return true
            return false
        })
    }
    if(startDate) {
        dataArray =dataArray.filter(it => {
            if(it.time >= startDate) return true
            return false
        })
    }
    if(endDate) {
        dataArray =dataArray.filter(it => {
            if(it.time <= endDate) return true
            return false
        })
    }
    return dataArray
}

app.use(express.json())
app.use(cors())
app.use(logger)
app.post("/ingest", (req, res, next) => {
    res.send("Success")
})
app.get("/ingest", (req, res, next) => {
    console.log("req.query", req.query)
    const fileData = fs.readFileSync(config.LOG_FILE_NAME, { encoding: "utf8" })
    res.json({ data: convertToJson(fileData, req.query.text, req.query.startDate, req.query.endDate) })
})
const server = createServer(app);
const io = new Server(server, {
    cors: {
        origin: "http://localhost:3000"
    },
    pingInterval: 25000,
    pingTimeout: 60000,
    wsEngine: ws.Server
});
new SocketClass(io)
server.listen(config.PORT, () => {
    console.log("Server listening on " + config.PORT)
})
