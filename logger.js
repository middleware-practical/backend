const fs = require('fs');
const { config } = require('./config');
const { SocketClass } = require('./socket');

const makeObjectToAppend = (body) => {
    const splitted = body.log.split("] ")
    const service = splitted[0].split(" ")
    return {
        time: body.time || Date.now().toString(),
        log: {
            "body": splitted[1],
            service: service.pop().replace("[", ""),
            severity: service[1],
            logTime: service[0]
        }
    }
}

const logger = (req, res, next) => {
    console.log("req.method", req.url)
    if(req.method.toLowerCase() === "post" && req.url === "/ingest") {
        console.log("fs.existsSync(config.LOG_FILE_NAME)", fs.existsSync(config.LOG_FILE_NAME))
        if (fs.existsSync(config.LOG_FILE_NAME)) {
            let obj = makeObjectToAppend(req.body)
            SocketClass.pushEvent(obj)
            fs.appendFile(config.LOG_FILE_NAME, JSON.stringify(obj) + "\n", function (err) {
                if (err) throw err;
                console.log('Saved!');
                next()
            });


        } else {
            let obj = makeObjectToAppend(req.body)
            SocketClass.pushEvent(obj)
            fs.writeFile(config.LOG_FILE_NAME, JSON.stringify(obj)+ "\n", (err) => {
                if (err) throw err;
                console.log('The file is created if not existing!!');
                next()
            });

        }
    } else {
        next()
    }
}

exports.logger = logger

