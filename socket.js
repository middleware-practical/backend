class SocketClass {
    static socketIo
    constructor(io) {
        SocketClass.socketIo = io
        
        io.on('connection', (socket) => {
            function joinLeave(rooms, method) {
                if (rooms !== null) {
                    if (method === 'join') {
                        socket.join(rooms);
                    } else {
                        socket.leave(rooms);
                    }
                } else {
                    logger.info('cannot get rooms, ignoring');
                }
            }
            console.log('a user connected');
            socket.on("subscribe", (rooms) => {
                console.log("\n\n\nrooms", rooms)
                joinLeave(rooms, "join")
            })
        });
      
        
    }

    static pushEvent(eventData) {
        SocketClass.socketIo.in("user").emit("push_data", JSON.stringify(eventData))
    }
}

exports.SocketClass = SocketClass